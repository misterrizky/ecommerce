<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'name' => 'Administrator',
                'username' => 'admin',
                'email' => 'admin@reverse.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('123456789'),
                'phone' => '087709020299',
                'role' => 'Admin',
                'address' => 'Jl. Talaga Bodas No. 29',
                'province_id' => '0',
                'city_id' => '0',
                'subdistrict_id' => '0',
                'postcode' => '40287'
            ],
        );
        foreach($data AS $d){
            User::create([
                'name' => $d['name'],
                'username' => $d['username'],
                'email' => $d['email'],
                'email_verified_at' => $d['email_verified_at'],
                'phone' => $d['phone'],
                'address' => $d['address'],
                'province_id' => $d['province_id'],
                'city_id' => $d['city_id'],
                'subdistrict_id' => $d['subdistrict_id'],
                'postcode' => $d['postcode'],
                'password' => $d['password']
            ]);
        }
    }
}
