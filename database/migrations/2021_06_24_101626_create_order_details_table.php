<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('product_id');
            $table->string('type',3);
            $table->string('titles');
            $table->string('price',20);
            $table->string('qty',5);
            $table->string('subtotal',20);
        });
    }
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
