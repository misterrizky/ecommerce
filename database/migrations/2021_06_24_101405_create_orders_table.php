<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('phone',15);
            $table->string('address');
            $table->integer('province_id');
            $table->integer('city_id');
            $table->integer('subdistrict_id');
            $table->string('postcode',6);
            $table->string('ekspedisi');
            $table->string('type');
            $table->string('notes')->nullable();
            $table->string('st');
            $table->string('photo');
            $table->string('resi',20)->nullable();
            $table->string('ongkir')->nullable();
            $table->string('total',20)->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
