<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderRatesTable extends Migration
{
    public function up()
    {
        Schema::create('order_rates', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('order_id');
            $table->float('rates');
            $table->longText('review');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('order_rates');
    }
}
