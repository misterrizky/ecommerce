<?php

namespace App\Http\Controllers;

use App\Models\OrderRate;
use Illuminate\Http\Request;

class OrderRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderRate  $orderRate
     * @return \Illuminate\Http\Response
     */
    public function show(OrderRate $orderRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderRate  $orderRate
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderRate $orderRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderRate  $orderRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderRate $orderRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderRate  $orderRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderRate $orderRate)
    {
        //
    }
}
