
    @foreach ($collection as $item)
    <div class="top-cart-item">
        <div class="top-cart-item-image">
            <a href="#">
                <img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" />
            </a>
        </div>
        <div class="top-cart-item-desc">
            <div class="top-cart-item-desc-title">
                <a href="#">Blue Round-Neck Tshirt with a Button</a>
                <span class="top-cart-item-price d-block">$19.99</span>
            </div>
            <div class="top-cart-item-quantity">x 2</div>
        </div>
    </div>
    @endforeach
