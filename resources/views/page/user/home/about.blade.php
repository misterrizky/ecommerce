<x-user-layout title="About">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body bg-warning">
                                <i class="icon-instagram" style="font-size:2rem;"></i>
                                <span> rvs_id</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body bg-warning">
                                <i class="icon-whatsapp-square" style="font-size:2rem;"></i>
                                <span>
                                    0821 - 1233 - 7831
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body bg-warning">
                                <i class="icon-map-marker1" style="font-size:2rem;"></i>
                                <span style="font-size:0.975em;">
                                    Jl. babakan sentral no 79 rw 05 rt 05 kelurahan sukapura kecamatan kiaracondong
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-user-layout>